<?php
// require_once 'HTTP/Request2.php';
require 'vendor/autoload.php';
$request = new HTTP_Request2();
$request->setUrl('https://virtserver.swaggerhub.com/ateliedepropaganda/register/1.0.0/costumer');
$request->setMethod(HTTP_Request2::METHOD_POST);
$request->setConfig(array(
  'follow_redirects' => TRUE
));
$request->setHeader(array(
  'Content-Type' => 'text/plain'
));
$request->setBody('{
	  "name": ' . $_POST['name'] . ',
	  "cpf": ' . $_POST['cpf'] . ',
	  "email": ' . $_POST['email'] . ',
	  "password": ' . $_POST['password'] . ',
	  "company": '. $_POST['company'] . ',
	  "classification": '. $_POST['classification'] . '
	}');
try {
  $response = $request->send();
  if ($response->getStatus() == 201) {
    echo "Cadastro realizado com sucesso!";
  }else if($response->getStatus() == 400){
  	echo "Existe um campo inválido, reveja seus dados";
  }else if($response->getStatus() == 409){
  	echo "Já existe um cadastro com esses dados";
  }
  else {
    echo "Ocorreu um erro, entre em contato com o administrador.";
  }
}
catch(HTTP_Request2_Exception $e) {
  echo 'Error: ' . $e->getMessage();
}